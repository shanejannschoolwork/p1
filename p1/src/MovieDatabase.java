///////////////////////////////////////////////////////////////////////////////
// Main Class File:  MovieFlix.java
// File:             MovieDatabase.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann  jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Stores Information passed to it by the MovieFlix.java class. Also contains 
 * methods capable of manipulating this information for the user. Exists
 * only for as long as MovieFlix is running.
 * 
 * <p> no known bugs
 * 
 * @author Shane
 */
public class MovieDatabase {

	ArrayList<Movie> movieList = new ArrayList<Movie>();

	/**
	 * Adds a movie to the database if it isn't a duplicate
	 * @param t title of the movie
	 */
	public void addMovie(String t){
		if(containsMovie(t)){

		}
		else{
			movieList.add(new Movie(t));
		}
	}

	/**
	 * Adds an actor to the cast of a given movie. If that movie cannot be
	 * found, an IllegalArgumentException is thrown and the program terminates.
	 * If the movie is found but already has the actor in its cast, the method
	 * returns. Otherwise, it adds the actor to the cast.
	 * 
	 * @param n The name of the actor
	 * @param t The title of the movie you are adding the actor to
	 */
	public void addActor(String n, String t){


		if(!containsMovie(t)){
			throw new IllegalArgumentException();
		}

		else{
			//If movie does exist, but the actor is already in 
			//the cast, returns
			if(findMovie(t).getCast().contains(n)){
			}

			//Otherwise, adds the actor to the cast
			else{
				findMovie(t).getCast().add(n.toLowerCase());
			}
		}
	}

	/**
	 * Removes the movie from the database if it can manage to find the movie.
	 * Otherwise, nothing happens.
	 * 
	 * @param t The title of the movie the user wishes to remove
	 * @return true if the movie is successfully removed, false otherwise
	 */
	public boolean removeMovie(String t){
		if(containsMovie(t)){
			movieList.remove(findMovie(t));
			return true;
		}
		return false;
	}

	/**
	 * Checks the database if it contains the given movie title.
	 * 
	 * @param t The title of the movie the user is checking
	 * @return true if movie is found, false otherwise
	 */
	public boolean containsMovie(String t){

		boolean hasMovie = false;

		for(int i = 0; i < movieList.size(); i++){
			if(t.equalsIgnoreCase(movieList.get(i).getTitle())){
				hasMovie = true;
			}
		}
		return hasMovie;

	}

	/**
	 * Checks the database if at least one movie lists the given actor in its
	 * cast. 
	 * 
	 * @param n The name of the actor the user is checking
	 * @return true if the actor is found in at least one movie, false
	 * otherwise
	 */
	public boolean containsActor(String n){

		Iterator<Movie> itr = iterator();

		while(itr.hasNext()){
			if(itr.next().getCast().contains(n
					
					)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if a given actor casts in a particular movie. 
	 * 
	 * @param n The name of the actor the user is checking
	 * @param t The title of the movie the user is checking the actor with
	 * @return true if the actor is indeed in the movie, false otherwise
	 */
	public boolean isCast(String n, String t){
		if(containsMovie(t)){
			if(findMovie(t).getCast().contains(n)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns, in list form, the cast of a given movie. This method will
	 * return null if a movie does not have any cast yet.
	 * 
	 * @param t The title of the movie with the desired cast
	 * @return returns a List<String> of the cast if the movie 
	 * is found (or null if there is no cast). Otherwise returns null
	 */
	public List<String> getCast(String t){
		if(containsMovie(t)){
			return findMovie(t).getCast();
		}
		return null;
	}

	/**
	 * returns a list of movies a specified actor was in. Will return null
	 * if the actor was not found in any movies in the database.
	 * 
	 * @param n The name of the actor 
	 * @return a List<String> of the movies the actor was in if he or she was
	 * found in the database.
	 */
	public List<String> getMovies(String n){
		List<String> actorMovies = new ArrayList<String>();
		Iterator<Movie> itr = iterator();
		int pos = 0;

		while(itr.hasNext()){
			if(itr.next().getCast().contains(n)){
				actorMovies.add(movieList.get(pos).getTitle());
			}
			pos++;
		}

		if(actorMovies.size() > 0){
			return actorMovies;
		}
		return null;
	}

	/**
	 * Returns a Movie Iterator for use
	 * @return Iterator<Movie>
	 */
	public Iterator<Movie> iterator(){
		return movieList.iterator();
	}

	/**
	 * Returns the number of movies in the database
	 */
	public int size(){
		return movieList.size();
	}

	/**
	 * Removes the desired actor from all the movies in the database.
	 * Returns false if the removal failed, true if successful.
	 * 
	 * @param n The name of the actor that is to be removed
	 * @return true if successful removal, false otherwise.
	 */
	public boolean removeActor(String n){
		boolean actorRemoved = false;

		for(int i = 0; i < movieList.size(); i++){
			if(isCast(n , movieList.get(i).getTitle())){
				movieList.get(i).getCast().remove(n);
				actorRemoved = true;
			}
		}

		return actorRemoved;
	}

	/**
	 * Used within this class to help individual methods traverse through
	 * the database without having to create a new iterator at each one. If 
	 * the movie is not found, return null.
	 * 
	 * @param t The title of the movie the method wishes to find
	 * @return the Movie object that the method desires. If the movie is not
	 * found, returns null
	 */
	private Movie findMovie(String t){
		Iterator<Movie> itr = iterator();

		while(itr.hasNext()){
			Movie currMovie = itr.next();
			if(currMovie.getTitle().equalsIgnoreCase(t)){
				return currMovie;
			}
		}
		return null;
	}
}

