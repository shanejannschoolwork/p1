///////////////////////////////////////////////////////////////////////////////
// Title:            MovieFlix.java
// Files:            MovieDatabase.java, Movie.java, MovieFlix.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann
// Email:            jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//////////////////////////// 80 columns wide //////////////////////////////////

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * This class interacts with MovieDatabase.java, it also reads in an
 * input file to act as the information to be put into the database.
 * Once read in, the class can display or alter the information within
 * the database in different ways
 *
 * <p>Bugs: none known
 *
 * @author Shane Jann
 */
public class MovieFlix {


	/**
	 * The main method prompts the user on what to do next, as well as
	 * handles the input the user submits. That input is fed into a switch
	 * statement where the input is processed and displayed as analyzed data.
	 * 
	 * @param args[0] will allow a file to be read in. 
	 * Only 1 argument is allowed
	 */
	public static void main(String[] args) {
		MovieDatabase database = new MovieDatabase();

		if(args.length == 1){
			String inputFile = args[0];
			try {
				fileReader(inputFile, database);
			} catch (FileNotFoundException e) {
				System.out.println("Cannot access input file");
				System.exit(0);
			}
		}
		else{
			System.out.println("Usage: java MovieFlix FileName");
			System.exit(0);
		}

		Scanner stdin = new Scanner(System.in);  //for console input

		boolean done = false;
		while (!done) {
			System.out.print("Enter option (cdprswx): ");
			String input = stdin.nextLine();

			//only do something if the user enters at least one character
			if (input.length() > 0) {
				char choice = input.charAt(0); //strip off option character
				String remainder = "";         //will hold the remaining input
				if (input.length() > 1) {      //if there is an argument
					//trim off any leading or trailing spaces
					remainder = input.substring(1).trim(); 

					switch (choice) { //the commands that have arguments

					//Displays the cast of a given movie title
					case 'c':
						if(database.containsMovie(remainder)){
							if(database.getCast(remainder).size() == 0){
								System.out.println("none");
							}
							else{
								System.out.println(listToString(
										database.getCast(remainder)));
							}
						}
						else{
							System.out.println("movie not found");
						}
						break;

						//prints the movies a given actor is casted in
					case 'p':
						if(database.containsActor(remainder.toLowerCase())){
							System.out.println(listToString(
									database.getMovies(
											remainder.toLowerCase())));
						}
						else{
							System.out.println("actor not found");
						}
						break;

						//removes a given movie title from the database
						//The user is then informed 
						//if the removal was succesful
					case 'r':
						if(database.containsMovie(remainder)){
							database.removeMovie(remainder);
							System.out.println("movie removed");
						}
						else{
							System.out.println("movie not found");
						}
						break;

						//searches the database for movies with both actors
						//casted in them.
					case 's':
						// The following code reads in a comma-separated
						//sequence  of strings.  If there are exactly
						//two strings in the sequence, the strings are 
						//assigned to name1 and name. Otherwise, an error
						//message is printed.
						String[] tokens = remainder.split("[,]+");
						if (tokens.length != 2) {
							System.out.println(
									"need to provide exactly two names");
						}
						else {
							String name1 = tokens[0].trim().toLowerCase();
							String name2 = tokens[1].trim().toLowerCase();
							ArrayList<String> moviesWithBoth = 
									new ArrayList<String>();



							Movie pointedTo = database.movieList.get(0);
							for(int i = 0; i < database.size(); i++){
								if(pointedTo.getCast().contains(name1) &&
										pointedTo.getCast().contains(name2)){

									if(!moviesWithBoth.contains(
											pointedTo.getTitle())){

										moviesWithBoth.add(
												pointedTo.getTitle());

									}
								}
							}
							if(moviesWithBoth.size() == 0){
								System.out.println("actor not found");
							}
							else{
								System.out.println(moviesWithBoth);
							}
						}
						break;

						//withdraws an actor from all movies
					case 'w':
						if(database.containsActor(remainder.toLowerCase())){
							database.removeActor(remainder.toLowerCase());
							System.out.println(
									"name withdrawn from all movies");
						}
						else{
							System.out.println("actor not found");
						}
						break;

					default: //ignore invalid commands
						System.out.println("Incorrect command.");
						break;

					} // end switch
				} // end if
				else { //if there is no argument
					switch (choice) { //the commands without arguments

					//displays five lines of analyzed data
					case 'd': 

						//Determines the number of unique actors
						ArrayList<String> uniqueActors = 
							new ArrayList<String>();
						Iterator<Movie> itr = database.iterator();
						Movie pointedTo = database.movieList.get(0);
						for( int i = 0; i < database.size(); i++){
							for(int j = 0; j < pointedTo.getCast()
									.size(); j++){

								if(uniqueActors.contains(pointedTo
										.getCast().get(j))){
									//won't add duplicate actors
								}
								else{
									uniqueActors.add(
											pointedTo.getCast().get(j));
								}

							} //end second for
							pointedTo = itr.next();
						} // end first for
						//end unique actors

						//Determines # of actors/movie
						int mostActors = pointedTo.getCast().size();
						int leastActors = pointedTo.getCast().size();
						int avgActors = pointedTo.getCast().size();
						itr = database.iterator();
						pointedTo = database.movieList.get(0);

						for(int i = 0; i < database.size(); i++){
							if(pointedTo.getCast().size() > mostActors){
								mostActors = pointedTo.getCast().size();
							}
							if(pointedTo.getCast().size() < leastActors){
								leastActors = pointedTo.getCast().size();
							}
							//add up actors
							avgActors += pointedTo.getCast().size(); 

							pointedTo = itr.next();
						}
						//divides all actors by amount of movies
						avgActors /= database.size();
						//end #actor/movie

						//Determines # of Movie/actor
						int mostMovies = 
								database.getMovies(uniqueActors.get(0)).size();
						int leastMovies = 
								database.getMovies(uniqueActors.get(0)).size();
						int avgMovies = 
								database.getMovies(uniqueActors.get(0)).size();
						for(int i = 0; i < uniqueActors.size(); i++){
							if(database.getMovies(uniqueActors.get(i)).size()
									> mostMovies){
								mostMovies = database.getMovies(
										uniqueActors.get(i)).size();
							}

							if(database.getMovies(uniqueActors.get(i)).size()
									< leastMovies){
								leastMovies = database.getMovies(
										uniqueActors.get(i)).size();
							}
							//adds up all the movies
							avgMovies += database.getMovies(
									uniqueActors.get(i)).size();
						}
						//divides by the number of actors
						avgMovies /= uniqueActors.size();
						//end #movie/actor

						//Determines largest and smallest casts
						ArrayList<String> largestCast =
								new ArrayList<String>();
						ArrayList<String> smallestCast = 
								new ArrayList<String>();
						pointedTo = database.movieList.get(0);
						itr = database.iterator();

						for(int i = 0; i < database.size(); i++){
							if(database.getCast(pointedTo.getTitle()).size() ==
									mostActors){

								//ensures no duplicates
								if(!largestCast.contains(
										pointedTo.getTitle())){
									largestCast.add(pointedTo.getTitle());
								}
							}
							if(database.getCast(pointedTo.getTitle()).size() ==
									leastActors){

								//ensures no duplicates
								if(!smallestCast.contains(
										pointedTo.getTitle())){
									smallestCast.add(pointedTo.getTitle());
								}
							}
							pointedTo = itr.next();
						}
						//end largest and smallest casts

						System.out.println("Movies: " + database.size() +
								", Actors: " + uniqueActors.size());

						System.out.println("# of actors/movie: most " + 
								mostActors + ", least " + leastActors +
								", average " + avgActors);

						System.out.println("# of movies/actor: most " + 
								mostMovies + ", least " + leastMovies +
								", average " + avgMovies);

						System.out.println("Largest Cast: " + 
								listToString(largestCast) + " [" + mostActors +
								"]");

						System.out.println("Smallest Cast: " +
								listToString(smallestCast) +
								" [" + leastActors + "]");

						break;

						//quits the program
					case 'x':
						done = true;
						System.out.println("exit");
						break;

					default:  //a command with no argument
						System.out.println("Incorrect command.");
						break;
					} //end switch
				} //end else  
			} //end if
		} //end while
	} //end main

	/**
	 * An auxiliary method that assists in changing lists to strings without
	 * added brackets that a regular toString would include
	 * 
	 * @param list the given list that needs to be changed
	 * @return a String form of the list
	 */
	private static String listToString(List<String> list){
		String result = "";
		for(int i = 0; i < list.size(); i++){
			result += list.get(i);
			if(i < list.size() - 1){
				result += ", ";
			}
		}
		return result;
	}

	/**
	 * An Auxiliary method that assists the main method in reading files.
	 * It also puts the file information into the correct format, then 
	 * adds that information into the database
	 * 
	 * @param fileName the actual name of the file
	 * @param database the database it will be adding to (starts out as empty)
	 * @throws FileNotFoundException
	 */
	private static void fileReader(String fileName, MovieDatabase database)
			throws FileNotFoundException{
		File file = new File(fileName);
		Scanner scnr = new Scanner(file);

		String input;

		while(scnr.hasNext()){
			input = scnr.nextLine();

			//splitInput[0] is the actor of the films
			//the remaining elements in the array are a films
			String[] splitInput = input.split(",");      
			String[] movieTitle;

			/*
			 * The first for loop below splits the movie titles into their
			 * individual words while the second for loop properly capitalizes
			 * those words and re-adds them into the splitInput array
			 */
			for(int i = 1; i < splitInput.length; i++ ){
				splitInput[i].toUpperCase();
				movieTitle = splitInput[i].split(" ");
				splitInput[i] = "";

				for(int j = 0; j < movieTitle.length; j++){
					if(movieTitle[j].length() > 0){
						movieTitle[j] = movieTitle[j].charAt(0) + movieTitle[j]
								.substring(1, movieTitle[j]
										.length()).toLowerCase();
					}
					
					splitInput[i] += movieTitle[j] + " ";
				}
				splitInput[i] = splitInput[i].trim();

			}

			//the actor and movies are added to the database
			for(int i = 1; i < splitInput.length; i++){
				database.addMovie(splitInput[i]);
				database.addActor(splitInput[0], splitInput[i]);
			}

		}

		scnr.close();
	}


}
